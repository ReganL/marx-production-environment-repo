��            )   �      �     �  9   �     �     �       	   !     +     2     L     ]     e  "  j     �     �     �  �   �     �  2   �  6   �  �     �   �     V     ]     t     �     �  �   �     V     j  W  �     �	  -   �	     
     *
     ?
     T
     b
     h
  (   �
     �
     �
    �
     �     �       �          S     0   r  �   �  �   U     
          )     ?     U  �   r          9                                                 	                                                                              
          + New A simple text widget to help promote Barley for WordPress Barley License Issue: Barley Update Error: Barley for WordPress Blog Post Delete Don't have a license key? Edit with Barley Notice: Page Please <a href="options-writing.php"><strong>enter a valid license key</strong></a> to use and keep Barley for WordPress up-to-date. To remove this message, <a href="plugins.php">deactivate the plugin</a>. Need help? <a href="http://support.getbarley.com/">Visit the Barley Help Center</a>. Promote Barley for WordPress Publish Purchase a License The Barley License Key you attempted to save is <em>invalid, not set up for this domain, or expired</em>. <strong>Please try again</strong> or <a href="http://support.getbarley.com">visit the Barley Help Center</a>. The Barley Team The inline editor for everyone. Now for WordPress. There is an update available for Barley for WordPress. There may be an update available. However, %1$s <a href="http://support.getbarley.com">Visit the Barley Help Center</a> to report or correct the issue. There seems to be a problem with your Barley License Key. %1$s <a href="http://support.getbarley.com">Visit the Barley Help Center</a> to report or correct the issue. Title: Turn Barley Editor off Turn Barley Editor on Visit Barley Help Center Watch Walkthrough Video Written using <a href="http://getbarley.com/" target="_blank" title="Barley, the inline editor for WordPress">Barley, the inline editor for WordPress</a>. Written with Barley http://getbarley.com/ Project-Id-Version: Barley for WordPress (beta) 0.3.3
Report-Msgid-Bugs-To: http://wordpress.org/tag/barley
POT-Creation-Date: 2013-11-28 15:47:15+00:00
PO-Revision-Date: 2013-11-28 16:51+0100
Last-Translator: 
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.5.7
 + Dodaj Prosty widget promujący Barley dla WordPress Problem z licencją Barley: Błąd aktualizacji: Barley dla Wordpress Wpis na blogu Usuń Nie posiadasz klucza produktu? Edytuj przy użyciu Barley dla Wordpress Informacja: Stronę Proszę <a href="options-writing.php"><strong>wprowadzić poprawny klucz produktu</strong></a> aby zaktualizować wtyczkę Barley dla WordPress. Aby usunąć tą wiadomość, <a href="plugins.php">wyłącz wtyczkę</a>. <a href="http://support.getbarley.com/">Potrzebujesz pomocy?</a> Promuj Barley dla WordPress Publikuj Kup licencję Klucz produktu jest <em>nieprawidłowy, nie skonfigurowany do współpracy z aktualną domeną, lub wygasł.</em> <strong>Prosimy, spróbuj ponownie</strong> lub <a href="http://getbarley.com/help"> skontaktuj się z naszą pomocą techniczną.</a> Ekipa Barley Prosty edytor treści dla każdego. Teraz dostępny także dla platformy WordPress. Dostępne nowe aktualizacje Barley dla Wordpress W trakcie pobierania aktualizacji wystąpił błąd.Szczegóły błędu: %1$s<a href="http://support.getbarley.com">Odwiedź centrum wsparcia Barley</a> aby zgłosić problem. Wystąpił problem z podanym kluczem licencyjnym Barley.Szczegóły błędu: %1$s<a href="http://support.getbarley.com">Odwiedź centrum wsparcia Barley</a> aby zgłosić problem. Tytuł: Wyłącz edytor Barley Włącz edytor Barley Centrum Pomocy Barley Obejrzyj film demonstracyjny Napisane przy użyciu <a href="http://getbarley.com/" target="_blank" title="Barley, the inline editor for WordPress">Barley, prostego edytora treści dla WordPress</a>. Napisane przy użyciu Barley http://getbarley.com/ 