��          �   %   �      p     q  9   w     �     �  	   �     �     �                  "  %     H     e     m  �   �     X  2   h  6   �  �   �  �   j               /     E     ^  �   v       �  %     �	  N   �	     
     +
     B
     O
     V
     l
  	   �
     �
     �
  !   �     �     �  �   �     �  G   �  (     �   E  �   �  	   _     i     �     �     �  �   �     y                                                                    	                                  
                     + New A simple text widget to help promote Barley for WordPress Barley License Issue: Barley Update Error: Blog Post Delete Don't have a license key? Edit with Barley Notice: Page Please <a href="options-writing.php"><strong>enter a valid license key</strong></a> to use and keep Barley for WordPress up-to-date. To remove this message, <a href="plugins.php">deactivate the plugin</a>. Need help? <a href="http://support.getbarley.com/">Visit the Barley Help Center</a>. Promote Barley for WordPress Publish Purchase a License The Barley License Key you attempted to save is <em>invalid, not set up for this domain, or expired</em>. <strong>Please try again</strong> or <a href="http://support.getbarley.com">visit the Barley Help Center</a>. The Barley Team The inline editor for everyone. Now for WordPress. There is an update available for Barley for WordPress. There may be an update available. However, %1$s <a href="http://support.getbarley.com">Visit the Barley Help Center</a> to report or correct the issue. There seems to be a problem with your Barley License Key. %1$s <a href="http://support.getbarley.com">Visit the Barley Help Center</a> to report or correct the issue. Title: Turn Barley Editor off Turn Barley Editor on Visit Barley Help Center Watch Walkthrough Video Written using <a href="http://getbarley.com/" target="_blank" title="Barley, the inline editor for WordPress">Barley, the inline editor for WordPress</a>. Written with Barley Project-Id-Version: Barley for WordPress 1.0.1
Report-Msgid-Bugs-To: http://wordpress.org/tag/barley
POT-Creation-Date: 2013-11-28 15:47:15+00:00
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2013-12-05 18:49+0100
Last-Translator: Zeray Rice <fanzeyi1994@gmail.com>
Language-Team: LANGUAGE <LL@li.org>
Language: zh_CN
X-Generator: Poedit 1.6.1
 + 新建 一个简单的文本小工具，用来帮助我们宣传 Barley for WordPress Barley 授权问题： Barley 更新错误： 博客文章 删除 还没有授权码？ 通过 Barley 编辑 提示： 页面 为了获得最新的 Barley for WordPress 更新，请<a href="options-writing.php"><strong>输入授权码</strong></a>。若想关闭此提示，请<a href="plugins.php">关闭本插件</a>。若需要帮助，请<a href="http://support.getbarley.com">访问 Barley 帮助中心</a>。 帮助宣传 Barley for WordPress 发布 购买一份授权 您试图保存的 Barley 授权码是<em>不正确的，未被设置为此域名的或已失效的</em>。<strong>请再试一次</strong>或<a href="http://support.getbarley.com">访问 Barley 帮助中心</a>。 Barley 团队 为每个人服务的行内编辑器，现已提供 WordPress 版本。 有新的 Barley for WordPress 更新。 似乎有新的更新，但是，%1$s <a href="http://support.getbarley.com">访问 Barley 帮助中心</a>以报告或解决此问题。 您的授权码似乎有点问题：%1$s 请<a href="http://support.getbarley.com">访问 Barley 帮助中心</a>以报告或解决此问题。 标题： 关闭 Barley 编辑器 开启 Barley 编辑器 访问 Barley 帮助中心 观看演示视频 使用<a href="http://getbarley.com/" target="_blank" title="Barley, 为 WordPress 打造的行内编辑器">Barley —— 为 WordPress 打造的行内编辑器</a>所撰写。 使用 Barley 撰写 