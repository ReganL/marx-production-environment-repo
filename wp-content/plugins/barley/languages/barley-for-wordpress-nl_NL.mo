��            )   �      �     �  9   �     �     �       	   !     +     2     L     ]     e  "  j     �     �     �  �   �     �  2   �  6   �  �     �   �     V     ]     t     �     �  �   �     V     j  �  �     *
  D   3
     x
     �
     �
     �
     �
     �
     �
            ,       F  
   f     q  �   �     p  1   �  8   �  �   �  �   �     I     P     h     �     �  �   �     Z     p                                                 	                                                                              
          + New A simple text widget to help promote Barley for WordPress Barley License Issue: Barley Update Error: Barley for WordPress Blog Post Delete Don't have a license key? Edit with Barley Notice: Page Please <a href="options-writing.php"><strong>enter a valid license key</strong></a> to use and keep Barley for WordPress up-to-date. To remove this message, <a href="plugins.php">deactivate the plugin</a>. Need help? <a href="http://support.getbarley.com/">Visit the Barley Help Center</a>. Promote Barley for WordPress Publish Purchase a License The Barley License Key you attempted to save is <em>invalid, not set up for this domain, or expired</em>. <strong>Please try again</strong> or <a href="http://support.getbarley.com">visit the Barley Help Center</a>. The Barley Team The inline editor for everyone. Now for WordPress. There is an update available for Barley for WordPress. There may be an update available. However, %1$s <a href="http://support.getbarley.com">Visit the Barley Help Center</a> to report or correct the issue. There seems to be a problem with your Barley License Key. %1$s <a href="http://support.getbarley.com">Visit the Barley Help Center</a> to report or correct the issue. Title: Turn Barley Editor off Turn Barley Editor on Visit Barley Help Center Watch Walkthrough Video Written using <a href="http://getbarley.com/" target="_blank" title="Barley, the inline editor for WordPress">Barley, the inline editor for WordPress</a>. Written with Barley http://getbarley.com/ Project-Id-Version: Barley for WordPress 1.0.1
Report-Msgid-Bugs-To: http://wordpress.org/tag/barley
POT-Creation-Date: 2013-11-28 15:47:15+00:00
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2013-11-28 19:30+0100
Last-Translator: Roy <royscheeren@gmail.com>
Language-Team: Roy Scheeren <hi@royscheeren.com>
X-Poedit-Language: Dutch
X-Poedit-Country: NETHERLANDS
 + Nieuwe Een simpele text widget om Barley voor WordPress te helpen promoten. Barley Licentie Probleem: Barley Update Fout: Barley voor WordPress Blog Bericht Verwijderen Heb je geen licentiesleutel? Bewerk met Barley Waarschuwing: Pagina  href="options-writing.php"><strong>Vul een geldige licentiesleutel in</strong></a> om Barley te gebruiken en up to date te houden. Om deze boodschap te verwijderen, <a href="plugins.php">deactiveer de plugin</a>. Hulp nodig? <a href="http://support.getbarley.com/">Bezoek het Barley Help Center</a>. Barley voor Wordpress promoten. Publiceren Koop een licentie De Barley licentiesleutel die je probeerde op te slaan is <em>ongeldig, niet ingesteld voor dit domein, of vervallen</em>. <strong>Probeer het opnieuw</strong> of <a href="http://support.getbarley.com">bezoek het Barley Help Center</a>. Het Barley Team De inline editor voor iedereen. Nu voor WordPress Er is een update voor Barley voor WordPress beschikbaar. Er kan een update beschikbaar zijn. Maar, %1$s <a href="http://support.getbarley.com">Bezoek het Barley Help Center</a> om het probleem te rapporteren of verhelpen. Er lijkt een probleem met je Barley Licentie Sleutel te zijn. %1$s <a href="http://support.getbarley.com">Bezoek het Barley Help Center</a> om het probleem te rapporteren of verhelpen. Titel: Barley Editor uitzetten Barley Editor aanzetten Bezoek Barley Help Center Bekijk Walkthrough Video Geschreven met behulp van <a href="http://getbarley.com/" target="_blank" title="Barley, de inline editor voor WordPress">Barley, de inline editor voor WordPress</a>. Geschreven met Barley http://getbarley.com/ 