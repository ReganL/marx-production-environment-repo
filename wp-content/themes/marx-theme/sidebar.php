<?php
/**
 * The Sidebar containing the side menu.
 *
 * @package _mbbasetheme
 */
?>
	<div id="toggle-menu" class="col-sm-12 col-xs-12 overlay-left opacity-none"></div>
	<nav class="navigation navigation--main navigation--open col-sm-6 col-xs-6" role="navigation">
		<div class="navigation-header">
			<div class="icon logo-marx-sm"></div>
		</div>
		<ul class="navigation-list list-unstyled">
			<li class="navigation-item navigation-item--active navigation-item--1">
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>">Home</a>
			</li>
			<li class="navigation-item navigation-item--2">
				<a href="<?php echo get_page_link(42); ?>">About</a>
			</li>
			<li class="navigation-item navigation-item--3">
				<a href="<?php echo get_page_link(43); ?>">Contact</a>
			</li>
			<li class="navigation-item navigation-item--4">
				<a href="<?php echo get_page_link(44); ?>">Archive</a>
			</li>
		</ul>
	</nav>
