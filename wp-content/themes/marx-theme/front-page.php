<?php
/**
 * Template Name: Home Page
 *
 * Displays content for home page layout
 *
 * @package _mbbasetheme
 */

get_header('home'); ?>
	
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php
			if ( is_page('Home') ) {
				query_posts( 'cat=2,3');
			}
			$i = 1;
			$j = 1;
			$k = 1;
			$news_query = new WP_Query('cat=3');
			$id_query = new WP_Query('cat=7,6');
			
			while ( have_posts() ) : the_post();

				if (in_category('2')):

					$image = get_field('thumbnail_1');
					$hover = get_field('thumbnail_2');
					$text_alt = get_field('text_alt');

					if( !empty($image) ): 

						// vars
						$url = $image['url'];
						$title = $image['title'];
						$alt = $image['alt'];
						$caption = $image['caption'];

						$url_2 = $hover['url'];
						$title_2 = $hover['title'];
						$alt_2 = $hover['alt'];
						$caption_2 = $hover['caption'];

						// thumbnail
						$size = 'large';
						$thumb = $image['sizes'][ $size ];
						$width = $image['sizes'][ $size . '-width' ];
						$height = $image['sizes'][ $size . '-height' ];

						$size_2 = 'large';
						$thumb_2 = $hover['sizes'][ $size ];
						$width_2 = $hover['sizes'][ $size . '-width' ];
						$height_2 = $hover['sizes'][ $size . '-height' ];


						$mobile = $image['sizes'][ 'sm' ];
						$original = $image['sizes'][ 'orig' ];
						$large = $image['url'];

						$mobile_2 = $hover['sizes'][ 'sm' ];
						$original_2 = $hover['sizes'][ 'orig' ];
						$large_2 = $hover['url'];

						if( $title ): ?>

							<div class="col-sm-12 col-md-6 item image-float">


								<div class="title-front-page">

									<?php endif; ?>

										<a href="<?php the_permalink(); ?>">
											<div id="animation-icon" class="animation"></div>
											<img class="thumb-main-1 lazy" data-mobile="<?php echo $mobile; ?>" data-original="<?php echo $original; ?>" data-large="<?php echo $large;?>" alt="<?php echo $alt; ?>"/>
											<img class="thumb-hover-2 lazy" data-mobile="<?php echo $mobile_2; ?>" data-original="<?php echo $original_2; ?>" data-large="<?php echo $large_2;?>" alt="<?php echo $alt_2; ?>"/>
										</a>

									<?php if( $title ): ?>

									<p class="title-front-page <?php if ($text_alt==TRUE): ?>blue<?php endif; ?>"><?php the_title(); ?></p>

								</div>
							</div>

						<?php endif; 
							$j = $j +1; 

							 while( $news_query->have_posts() ) : $news_query->the_post();
								if(($j === 8) || ($j === 14) || ($j === 22)): 
									$string = get_field('introduction');
									$sub_string = substr($string,0,155).'...';?>
								<div class="col-sm-12 col-md-6 item">
									<a href="<?php the_permalink(); ?>">
										<div class="rectangle-home-text col-md-12 <?php if ($k === 1 || $k === 3): ?>inverse-content<?php endif ;?>">
											<div class="body-h-block">
												<h4><?php echo $sub_string; ?></h4>
											</div>
										</div>
									</a>
								</div>

								<?php $i= $i + 1;
									  $j = $j +1;
									  $k = $k + 1; 
								endif ; 
							endwhile; 
							 if($j === 11): $id_counter = 1;
								while( $id_query->have_posts() ) : $id_query->the_post(); ?>
								<div class="col-sm-12 col-md-6 item image-float identity-overflow">
									<div class="title-front-page margin-id">
										<a href="<?php the_permalink(); ?>">
											<?php while ( have_rows("home_id")) : the_row(); 
												 $id_image = get_sub_field('identity');

														if (!empty($id_image)):

														$url_id = $id_image['url'];
														$title_id = $id_image['title'];
														$alt_id = $id_image['alt'];
														$caption_id = $id_image['caption'];

														$size = 'large';
														$thumb_id = $id_image['sizes'][ $size ];
														$width_id = $id_image['sizes'][ $size . '-width' ];
														$height_id = $id_image['sizes'][ $size . '-height' ];

														$mobile_id = $id_image['sizes'][ 'sm' ];
														$original_id = $id_image['sizes'][ 'original' ];

													?>
													<img id="slide-<?php echo $id_counter; ?>" class="slide-identity lazy<?php if($id_counter === 1):?> visible-slide<?php endif; ?>" data-mobile="<?php echo $mobile_id; ?>" data-original="<?php echo $original_id; ?>" data-large="<?php echo $url_id; ?>"  alt="<?php echo $alt_id; ?>"/>

												<?php $id_counter=$id_counter+1;
													endif; endwhile; ?>
											</a>
										<p id="title-identity" class="<?php if ($text_alt == True):?>blue<?php endif;?>"><?php echo get_cat_name('7'); ?></p>
									</div>
								</div>
							<?php endwhile; 
						 endif; 
					 endif; 
				 endif; 
			 endwhile; // end of the loop. ?>
		</main><!-- #main -->
	</div><!-- #primary -->


<?php get_sidebar(); ?>
<?php get_footer(); ?>
