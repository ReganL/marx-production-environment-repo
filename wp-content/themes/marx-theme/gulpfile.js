// Load plugins
var gulp = require('gulp'),
	clean = require('gulp-clean'),
	plugins = require('gulp-load-plugins')({ camelize: true }),
	sass = require('gulp-ruby-sass'),
	lr = require('tiny-lr'),
	server = lr(),
	files = require('./build.config.js').files,
	svgmin = require('gulp-svgmin'),
	svgSprite = require("gulp-svg-sprites"),
	filter = require('gulp-filter'),
	rename = require('gulp-rename'),
	runSequence = require('run-sequence'),
	// raster = require('gulp-raster'),
	// iconfont = require('gulp-iconfont'),
	consolidate = require('gulp-consolidate');
	// fontcustom = require('gulp-fontcustom');

// Styles
gulp.task('styles', function() {
	return gulp.src('assets/styles/*.scss')
	.pipe(sass())
	// .pipe(plugins.sass({ style: 'expanded', sourcemap: true }))
	.pipe(plugins.autoprefixer('last 2 versions', 'ie 9', 'ios 6', 'android 4'))
	.pipe(gulp.dest('assets/styles/build'))
	.pipe(plugins.minifyCss({ keepSpecialComments: 1 }))
	.pipe(plugins.livereload(server))
	.pipe(gulp.dest('./'))
	.pipe(plugins.notify({ message: 'Styles task complete' }));
});



// Vendor Plugin Scripts
gulp.task('plugins', function() {
	return gulp.src(files.js.vendor)
	.pipe(plugins.concat('plugins.js'))
	.pipe(gulp.dest('assets/js/build'))
	.pipe(plugins.rename({ suffix: '.min' }))
	.pipe(plugins.uglify())
	.pipe(plugins.livereload(server))
	.pipe(gulp.dest('assets/js'))
	.pipe(plugins.notify({ message: 'Scripts task complete' }));
});

// Site Scripts
gulp.task('scripts', function() {
	return gulp.src(['assets/js/source/*.js', '!assets/js/source/plugins.js'])
	.pipe(plugins.jshint('.jshintrc'))
	.pipe(plugins.jshint.reporter('default'))
	.pipe(plugins.concat('main.js'))
	.pipe(gulp.dest('assets/js/build'))
	.pipe(plugins.rename({ suffix: '.min' }))
	.pipe(plugins.uglify())
	.pipe(plugins.livereload(server))
	.pipe(gulp.dest('assets/js'))
	.pipe(plugins.notify({ message: 'Scripts task complete' }));
});

// Images
// gulp.task('images', function() {
//   return gulp.src('assets/images/**/*')
// 	.pipe(plugins.cache(plugins.imagemin({ optimizationLevel: 7, progressive: true, interlaced: true })))
// 	.pipe(plugins.livereload(server))
// 	.pipe(gulp.dest('assets/images'))
// 	.pipe(plugins.notify({ message: 'Images task complete' }));
// });

// Clean minified SVGS. Prevents deleted svgs from being added to the sprite.
gulp.task('clean-svgs', function () {
	return gulp.src('assets/images/svgs/dest', {read: false})
		.pipe(clean());
});

// Minify SVGs
gulp.task('svgmin', ['clean-svgs'], function() {
	return gulp.src('assets/images/svgs/src/*.svg')
		.pipe(svgmin())
		.pipe(gulp.dest('assets/images/svgs/dest/'));
});

//Process svgs into sprites and generate scss file
var config = {
	cssFile: "scss/_sprite.scss",
	svgPath: "assets/images/sprite.svg",
	pngPath: "assets/images/sprite.png",
	preview: false,
	templates: {
		css: require("fs").readFileSync("./assets/templates/sprite.css", "utf-8")
	}
};

//Process svgs into sprites and generate scss file
// gulp.task('sprites', function () {
// 	return gulp.src('assets/images/svgs/dest/*.svg')
// 		.pipe(svgSprite(config))
// 		.pipe(gulp.dest('assets/styles/sprites')) // Write the sprite-sheet + SCSS + Preview
// 		.pipe(filter('**/*.svg')) // Filter out everything except the SVG file
// 		.pipe(raster())
// 		.pipe(rename({extname: '.png'}))
// 		.pipe(gulp.dest('assets/styles/sprites'));
// });

// Process images.
gulp.task('img', function () {
	return gulp.src(files.img.src)
		.pipe(rename({dirname: ''}))
		.pipe(gulp.dest(files.img.buildDest));
});

//Generating fonts for amimations
// gulp.task('Iconfont', function(){
// 	gulp.src(['assets/images/animation/src/monogram/*.svg'])
// 		.pipe(iconfont({
// 			fontName: 'monogram', // required
// 			appendCodepoints: true // recommended option
// 		}))
// 		.on('codepoints', function(codepoints, options) {
// 			// CSS templating, e.g.
// 			console.log(codepoints, options);
// 		})
// 		.pipe(gulp.dest('assets/images/animation/build'));
// });

// gulp.task('Iconfont', function(){
//   gulp.src(['assets/images/animation/src/monogram/*.svg'])
//     .pipe(iconfont({ fontName: 'monogram' }))
//     .on('codepoints', function(codepoints, options) {
//       gulp.src('assets/templates/fontawesome-style.css')
//         .pipe(consolidate('lodash', {
//           glyphs: codepoints,
//           fontName: 'monogram',
//           fontPath: '../fonts/',
//           className: 's'
//         }))
//         .pipe(gulp.dest('assets/images/animation/build'));
//     })
//     .pipe(gulp.dest('assets/images/animation/build'));
// });

// Process icon font.
// gulp.task('iconFont', function () {
// 	return gulp.src("assets/images/animation/src/monogram/*.svg")
// 		.pipe(fontcustom({
// 		font_name: 'monogram'  // defaults to 'fontcustom'
// 	}))
// 	.pipe(gulp.dest("assets/images/animation/build"))
// });

// Watch
gulp.task('watch', function() {

	// Listen on port 35729
	server.listen(35729, function (err) {
		if (err) {
			return console.log(err)
		};

		// Watch .scss files
		gulp.watch('assets/styles/**/*.scss', ['styles']);

		// Watch .js files
		gulp.watch('assets/js/**/*.js', ['plugins', 'scripts']);

	});

});

// Default task
gulp.task('default', function (callback) {
	runSequence(
	'plugins', 
	'scripts', 
	// 'images', 
	// 'Iconfont',
	// 'iconFont',
	'watch', 
	// 'svgmin',
	// 'sprites',
	[
		'styles', 
		'img'
	],
	callback);
})