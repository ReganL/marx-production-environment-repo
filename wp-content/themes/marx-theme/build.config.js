module.exports = {
	files: {
		js: {

			// Use uncompressed versions of 3rd-pary libraries.
			// They will be compressed in production.
			// Any libraries added to /vendor must be added here.
			// If you remove a library you must remove it here too.
			vendor: [
				'assets/js/source/plugins.js',
				'assets/js/vendor/jquery/dist/jquery.js',
				'assets/js/vendor/customizer.js',
				'assets/js/vendor/navigation.js',
				'assets/js/vendor/skip-link-focus-fix.js',
				'assets/js/vendor/jquery.lazyload/jquery.lazyload.js'
				// 'assets/js/vendor/jquery.lazyload/jquery.lazyload.js'
			],
			buildDest: './assets/js/build/js'
		},
		img: {
			src: [
				'assets/styles/sprites/svg/*.svg',
				'assets/styles/sprites/svg/*.png'
			],
			buildDest: './assets/images'
		},
		fonts: {
			src: [
				'assets/fonts/source/*.ttf',
			],
			buildDest: 'assets/fonts/build'
		}
	}
}

