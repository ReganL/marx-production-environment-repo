# Marx Theme

Version: 1.0

## Authors:

Douglas Bamber/Regan Langford

## Summary

Theme to manage the Marx Design Website

## Usage

The theme uses gulp tasks to manage styles, JS and SVG sprite sheet compilation with PNG fallbacks

Rename folder to your theme name, change the `style.scss` intro block to your theme information. Open the theme directory in terminal and run `npm install` to pull in all Gulp dependencies. Run `gulp` to execute tasks. Code as you will.

- Compile `assets/styles/style.scss` to `style.css`
- Compile `assets/styles/style.scss` to `style.css`
- Concatenate and minify plugins in `assets/js/vendor` and `assets/js/source/plugins.js` to `assets/js/plugins.min.js`
- Minify and lint `assets/js/source/main.js` to `assets/js/main.min.js`
- All SVGs in the SVG image folder will be compiled into a sprite sheet. You can call them with classes that match the SVG name.

To concatenate and minify your jQuery plugins, add them to the `assets/js/vendor` directory and add the `js` filename and path to the `gulp` `uglify` task. Previous versions of the starter theme automatically pulled all plugins in the `vendor` directory, but this has changed to allow more granular control and for managing plugins and assets with bower.

### Features

1. Normalized stylesheet for cross-browser compatibility using Normalize.css version 3 (IE8+)
2. Easy to customize
3. Bootstrap framework
4. Media Queries can be nested in each selector using SASS
5. SCSS with plenty of mixins ready to go
6. Gulp for processing all SASS, JavaScript and images

### Suggested Plugins

* [Use Google Libraries](http://wordpress.org/extend/plugins/use-google-libraries/)
* [WordPress SEO by Yoast](http://wordpress.org/extend/plugins/wordpress-seo/)
* [Google Analytics for WordPress by Yoast](http://wordpress.org/extend/plugins/google-analytics-for-wordpress/)
* [W3 Total Cache](http://wordpress.org/extend/plugins/w3-total-cache/)
* [Gravity Forms](http://www.gravityforms.com/)


### Credits

* [Wordpress Starter Theme](https://github.com/mattbanks/WordPress-Starter-Theme)
* [HTML5 Boilerplate](http://html5boilerplate.com)
* [Normalize.css](http://necolas.github.com/normalize.css)
* [SASS / SCSS](http://sass-lang.com/)
* [AutoPrefixr](https://github.com/ai/autoprefixer)
* [Don't Overthink It Grids](css-tricks.com/dont-overthink-it-grids/)
* [Underscores Theme](https://github.com/Automattic/_s)
* [Gulp JS] (http://gulpjs.com/)
