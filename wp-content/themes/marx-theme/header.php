<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package _mbbasetheme
 */
?>
<!DOCTYPE html>
<html class="no-js" <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?php wp_title( '|', true, 'right' ); ?></title>

	<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicons/favicon.ico">
	<!-- <link rel="apple-touch-icon" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/null/apple-touch-icon.png"> -->
	<link rel="apple-touch-icon" sizes="57x57" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicons/apple-touch-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicons/apple-touch-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicons/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="144x144" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicons/apple-touch-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="60x60" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicons/apple-touch-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicons/apple-touch-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicons/apple-touch-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicons/apple-touch-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicons/apple-touch-icon-180x180.png">
	<link rel="icon" type="image/png" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicons/favicon-192x192.png" sizes="192x192">
	<link rel="icon" type="image/png" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicons/favicon-160x160.png" sizes="160x160">
	<link rel="icon" type="image/png" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicons/favicon-96x96.png" sizes="96x96">
	<link rel="icon" type="image/png" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicons/favicon-16x16.png" sizes="16x16">
	<link rel="icon" type="image/png" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicons/favicon-32x32.png" sizes="32x32">
	<meta name="msapplication-TileColor" content="#f7fafc">
	<meta name="msapplication-TileImage" content="/mstile-144x144.png">

	<script src="//use.typekit.net/rvc1zil.js"></script>
	<script>try{Typekit.load();}catch(e){}</script>
	<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>
<div id="page" class="hfeed site tk-nimbus-sans">
	<!--[if lt IE 9]>
	    <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
	<![endif]-->

	<a class="skip-link screen-reader-text" href="#content"><?php _e( 'Skip to content', '_mbbasetheme' ); ?></a>

	<header id="masthead" class="site-header" role="banner">

		<a href="<?php echo esc_url( home_url( '/' ) ); ?>">
			<div class="inverse-content site-branding col-sm-6">
				<div class="icon marx-brand-lg"></div>
				<div class="icon marx-brand-lg-hover"></div>


				<div class="marx-tagline-container">
					<p>
					<?php

					$rows = get_field('taglines', 'option');
					$row_count = count($rows);
					$i = rand(0, $row_count - 1);

					echo $rows[ $i ]['tagline'];

					?>
					</p>

				</div>

				<div class="home-container">
					<p>Home</p>
				</div>
				
			</div>
		</a>

		<nav id="site-navigation" class="main-navigation col-sm-6 hidden-xs" role="navigation">
			<?php wp_nav_menu( array( 'theme_location' => 'primary' ) ); ?>

		</nav><!-- #site-navigation -->

		<div class="menu-container">
			<div id="menu-open" class="icon menu-fixed menu-hide"></div>
			<div id="menu-open-mob" class="icon menu-mobile"></div>
		</div>

		<?php if(is_page("Archive")): ?>
			<div class="col-sm-12 col-md-6 right-tagline">
				<div class="rectangle-home-text">
					<div class="body-h-block">
						<h4><?php the_field("introduction_archive"); ?></h4>
					</div>
				</div>
			</div>
		<?php endif; ?>

	</header><!-- #masthead -->

	<div id="content" class="site-content">
