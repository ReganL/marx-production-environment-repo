<?php
/**
 * The Template for displaying all single posts.
 *
 * @package _mbbasetheme
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php 
			$image = get_field('image_header');

			$url_in = $image['url'];
			$title_in = $image['title'];
			$alt_in = $image['alt'];
			$caption_in = $image['caption'];

			$size = 'large';
			$thumb = $image['sizes'][ $size ];
			$width = $image['sizes'][ $size . '-width' ];
			$height = $image['sizes'][ $size . '-height' ];

			$mobile_in = $image['sizes'][ 'sq-mobile' ];
			$original_in = $image['sizes'][ 'sq-original' ];
			$large_in = $image['url'];

			if( !in_category("7") && ( !in_category("6") ) ): ?>
			<div class="col-sm-12 col-md-6">



				<img class="thumb-main-1 lazy" data-mobile="<?php echo $mobile_in; ?>" data-original="<?php echo $original_in; ?>" data-large="<?php echo $url_in; ?>" alt="<?php echo $alt; ?>" />

			</div>
		<?php endif; ?>


		<?php 
			$intro = get_field("introduction");
			if(!in_category("7")  && !in_category("6") ): ?>
			<div class="col-md-6 col-sm-12">
				<div class="square-text">
					<div class="body-h-block">
						<h2><?php the_title(); ?></h2>
						<p><?php the_field("introduction"); ?></p>
					</div>
				</div>
			</div>
		<?php endif;?>

 		<?php if ( have_rows("template_flexibility") ) : ?>

			<?php while ( have_rows("template_flexibility")) : the_row(); ?>
				<?php  
					$template = get_field("template_flexibility");
					$rows = $templates[$x];

					$image_full = get_sub_field("image_full_page");

					$text_left = get_sub_field("text_half_1");
					$image_right = get_sub_field("image_half_1");

					$image_left = get_sub_field("image_half_2");
					$text_right = get_sub_field("text_half_2");
					$text_full = get_sub_field("large_text");
					$alternate_background = get_sub_field("alternate_background");

					$url = $image_full['url'];
					$title = $image_full['title'];
					$alt = $image_full['alt'];
					$caption = $image_full['caption'];

					// thumbnail
					$size = 'large';
					$thumb = $image['sizes'][ $size ];
					$width = $image['sizes'][ $size . '-width' ];
					$height = $image['sizes'][ $size . '-height' ];

					$mobile = $image_full['sizes'][ 'sm' ];
					$original = $image_full['sizes'][ 'orig' ];

					$url_left = $image_left['url'];
					$title_left = $image_left['title'];
					$alt_left = $image_left['alt'];
					$caption_left = $image_left['caption'];

					$mobile_left = $image_left['sizes'][ 'sq-mobile' ];
					$original_left = $image_left['sizes'][ 'sq-original' ];

					$url_right = $image_right['url'];
					$title_right = $image_right['title'];
					$alt_right = $image_right['alt'];
					$caption_right = $image_right['caption'];

					$mobile_right = $image_right['sizes'][ 'sq-mobile' ];
					$original_right = $image_right['sizes'][ 'sq-original' ];

					if ($image_full): ?>

						<div class="col-sm-12">
							<img class="image-responsive lazy" data-mobile="<?php echo $mobile; ?>" data-original="<?php echo $original; ?>" data-large="<?php echo $url; ?>" alt="<?php echo $alt; ?>"/>
						</div>

					<?php elseif ( ($image_right) && ($text_left) ): ?>

						<div class="col-md-6 col-sm-12 case-clear">
							<div class="square-text <?php if ($alternate_background===TRUE): ?>inverse-content<?php endif; ?>">
								<div class="body-h-block">
									<p><?php echo $text_left; ?></p>
								</div>
							</div>
						</div>

						<div class="col-md-6 col-sm-12">
							<img class="image-responsive lazy" data-mobile="<?php echo $mobile_right; ?>" data-original="<?php echo $original_right; ?>" data-large="<?php echo $url_right;?>"  alt="<?php echo $alt_right; ?>"/>
						</div>

					<?php elseif ( ($image_left) && ($text_right) ): ?>

						<div class="col-md-6 col-sm-12">
							<img class="image-responsive lazy" data-mobile="<?php echo $mobile_left; ?>" data-original="<?php echo $original_left; ?>" data-large="<?php echo $url_left;?>"  alt="<?php echo $alt_left; ?>"/>
						</div>

						<div class="col-md-6 col-sm-12">
							<div class="square-text <?php if ($alternate_background===TRUE): ?>inverse-content<?php endif; ?>">
								<div class="body-h-block">
									<p><?php echo $text_right; ?></p>
								</div>
							</div>
						</div>

					<?php elseif ( ($image_left)  && ($image_right)) : ?>
						<div class="col-sm-6 case-clear">
							<img class="image-responsive lazy" data-mobile="<?php echo $mobile_left; ?>" data-original="<?php echo $original_left; ?>" data-large="<?php echo $url_left;?>"  alt="<?php echo $alt_left; ?>"/>
						</div>
						<div class="col-sm-6">
							<img class="image-responsive lazy" data-mobile="<?php echo $mobile_right; ?>" data-original="<?php echo $original_right; ?>" data-large="<?php echo $url_right;?>"  alt="<?php echo $alt_right; ?>"/>
						</div>
					<?php elseif($text_full): ?>
						<div class="col-sm-12 large-text-rectangle <?php if ($alternate_background===TRUE): ?>inverse-content<?php endif; ?>">
							<div class="lg-text-container">
								<h5><?php echo $text_full ;?></h5>
							</div>
						</div>

				<?php endif; ?>
			<?php endwhile; ?>
		<?php endif; ?>


		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
	<div class="col-sm-12 col-xs-12">


			<div class="col-sm-6 col-xs-6 prev-container case-study-nav inverse-content">
				<?php if (in_category("3")):
					next_post_link('%link', 'Previous&nbsp;Project', TRUE ); 
				elseif (!in_category("3")):
						next_post_link('%link', 'Previous&nbsp;Project', FALSE, '3' ); 
				endif;?>
			</div>

			<div class="col-sm-6 col-xs-6 next-container case-study-nav inverse-content">
				<?php if (in_category("3")):?>
					<?php previous_post_link('%link', 'Next&nbsp;Project', TRUE); ?>
				<?php endif;?>
				<?php if (!in_category("3")):?>
					<?php previous_post_link('%link', 'Next&nbsp;Project', FALSE, '3'); ?>
				<?php endif;?>
			</div>

	</div>
<?php get_footer(); ?>