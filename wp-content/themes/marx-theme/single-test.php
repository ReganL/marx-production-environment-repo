<?php
/**
 * The Template for displaying all single posts.
 *
 * @package _mbbasetheme
 */

get_header(); ?>


	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php 

		$image = get_field('image_header');

		if( !empty($image) ): ?>
			<div class="col-sm-12 col-md-6">


				<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				<!-- <img class="thumb-main-1 lazy" data-mobile="<?php echo $mobile; ?>" data-original="<?php echo $original; ?>" alt="<?php echo $alt; ?>" /> -->

			</div>
		<?php endif; ?>

		<?php 
			$intro = get_field('introduction');
			if( !empty( $intro )): ?>

			<div class="col-md-6 col-sm-12">
				<div class="square-text">
					<div class="body-h-block">
						<h2><?php echo the_title(); ?></h2>
						<p><?php $intro; ?></p>
					</div>
				</div>
			</div>
		<?php endif;?>

 		<?php if ( have_rows("template_flexibility") ) : ?>

			<?php while ( have_rows("template_flexibility")) : the_row(); ?>
				<?php  
					$template = get_field("template_flexibility");
					$rows = $templates[$x];

					$image_full = get_sub_field("image_full_page");

					$text_left = get_sub_field("text_half_1");
					$image_right = get_sub_field("image_half_1");

					$image_left = get_sub_field("image_half_2");
					$text_right = get_sub_field("text_half_2");
					$text_full = get_sub_field("large_text");
					$alternate_background = get_sub_field("alternate_background");


					if ($image_full): ?>

						<div class="col-sm-12">
							<img class= "image-responsive" src="<?php echo $image_full['url']; ?>" alt="<?php echo $image_full['alt']; ?>"/>
						</div>

					<?php elseif ( ($image_right) && ($text_left) ): ?>

						<div class="col-md-6 col-sm-12 case-clear">
							<div class="square-text <?php if ($alternate_background===TRUE): ?>inverse-content<?php endif; ?>">
								<div class="body-h-block">
									<p><?php echo $text_left; ?></p>
								</div>
							</div>
						</div>

						<div class="col-md-6 col-sm-12">
							<img class="image-responsive" src="<?php echo $image_right['url']; ?>" alt="<?php echo $image_right['alt']; ?>" />
						</div>

					<?php elseif ( ($image_left) && ($text_right) ): ?>

						<div class="col-md-6 col-sm-12 ">
							<img class="image-responsive" src="<?php echo $image_left['url']; ?>" alt="<?php echo $image_left['alt']; ?>" />
						</div>

						<div class="col-md-6 col-sm-12">
							<div class="square-text <?php if ($alternate_background===TRUE): ?>inverse-content<?php endif; ?>">
								<div class="body-h-block">
									<p><?php echo $text_right; ?></p>
								</div>
							</div>
						</div>

					<?php elseif ( ($image_left)  && ($image_right)) : ?>
						<div class="col-sm-6">
							<img class="image-responsive" src="<?php echo $image_left['url']; ?>" alt="<?php echo $image_left['alt']; ?>" />
						</div>

						<div class="col-sm-6">
							<img class="image-responsive" src="<?php echo $image_right['url']; ?>" alt="<?php echo $image_right['alt']; ?>" />
						</div>
					<?php elseif($text_full): ?>
						<div class="col-sm-12 large-text-rectangle <?php if ($alternate_background===TRUE): ?>inverse-content<?php endif; ?>">
							<div class="lg-text-container">
								<h5><?php echo $text_full ;?></h5>
							</div>
						</div>

				<?php endif; ?>
			<?php endwhile; ?>
		<?php endif; ?>


		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
	<div class="col-sm-12 col-xs-12">


			<div class="col-sm-6 col-xs-6 prev-container case-study-nav inverse-content">
				<?php next_post_link('%link', 'Previous&nbsp;Project', TRUE ); ?>
			</div>

			<div class="col-sm-6 col-xs-6 next-container case-study-nav inverse-content">
				<?php previous_post_link('%link', 'Next&nbsp;Project', TRUE); ?>
			</div>

	</div>
<?php get_footer(); ?>