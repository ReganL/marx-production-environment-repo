<?php
/**
 * Template Name: Archive Page
 *
 * Displays content for about page layout
 *
 * @package _mbbasetheme
 */

get_header(); ?>

	<section id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php
			if ( is_page("Archive") ) {
				query_posts( 'cat=-2,-3,-6,-7' );
			}
			?>


			<?php while ( have_posts() ) : the_post(); ?>

				<div class="col-sm-12 col-md-6  clear-archive">
					<?php 

					$image = get_field('thumbnail_1');
					// $hover = get_field('thumbnail_2');

					if( !empty($image) ): 

						// vars
						$url = $image['url'];
						$title = $image['title'];
						$alt = $image['alt'];
						$caption = $image['caption'];

						// $url_2 = $hover['url'];
						// $title_2 = $hover['title'];
						// $alt_2 = $hover['alt'];
						// $caption_2 = $hover['caption'];

						// thumbnail
						$size = 'large';
						$thumb = $image['sizes'][ $size ];
						$width = $image['sizes'][ $size . '-width' ];
						$height = $image['sizes'][ $size . '-height' ];

						// $size_2 = 'large';
						// $thumb_2 = $hover['sizes'][ $size ];
						// $width_2 = $hover['sizes'][ $size . '-width' ];
						// $height_2 = $hover['sizes'][ $size . '-height' ];

						$mobile = $image['sizes'][ 'sm' ];
						$original = $image['sizes'][ 'orig' ];

						if( $title ): ?>

						<div class="title-front-page">

						<?php endif; ?>

							<a href="<?php the_permalink(); ?>">

								<img class="thumb-main-1 lazy" data-mobile="<?php echo $mobile; ?>" data-original="<?php echo $original; ?>" data-large="<?php echo $url; ?>" alt="<?php echo $alt; ?>" />
							</a>

						<?php if( $title ): ?>

							<p class="title-front-page"><?php the_title(); ?></p>

						</div>

						<?php endif; ?>

					<?php endif; ?>
				</div>
			<?php endwhile; // end of the loop. ?>






		</main><!-- #main -->
	</section><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
