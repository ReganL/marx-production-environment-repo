<?php
/**
 * Template Name: Home Page
 *
 * Displays content for home page layout
 *
 * @package _mbbasetheme
 */

get_header('home'); ?>
	
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php
			if ( is_page("Home") ) {
				query_posts( 'cat=2,3');
			}
			$i = 1;
			$j = 1;
			$k = 1;
			?>

			<?php while ( have_posts() ) : the_post(); ?>
					<?php if ($j === 2): ?>
<!-- This section ensures masonry can function with the tagline in the second block -->

						<div class="col-sm-12 col-md-6 right-tagline item">
							<div class="rectangle-home-text">
								<div class="body-h-block">
									<h4><?php echo get_bloginfo( 'description' ); ?></h4>
								</div>
							</div>
						</div>
					<?php endif; ?>

					<div class="col-sm-12 col-md-6 item news-width">

						<?php if (in_category('2')): ?>

							<?php 

							$image = get_field('thumbnail_1');
							// $hover = get_field('thumbnail_2');

							if( !empty($image) ): 

								// vars
								$url = $image['url'];
								$title = $image['title'];
								$alt = $image['alt'];
								$caption = $image['caption'];

								// $url_2 = $hover['url'];
								// $title_2 = $hover['title'];
								// $alt_2 = $hover['alt'];
								// $caption_2 = $hover['caption'];

								// thumbnail
								$size = 'large';
								$thumb = $image['sizes'][ $size ];
								$width = $image['sizes'][ $size . '-width' ];
								$height = $image['sizes'][ $size . '-height' ];

								// $size_2 = 'large';
								// $thumb_2 = $hover['sizes'][ $size ];
								// $width_2 = $hover['sizes'][ $size . '-width' ];
								// $height_2 = $hover['sizes'][ $size . '-height' ];

								if( $title ): ?>

								<div class="title-front-page">

								<?php endif; ?>

									<a href="<?php the_permalink(); ?>" title="<?php echo $title; ?>">
										<div id="animation-icon" class="animation icon"></div>
										<img class="thumb-main-1" src="<?php echo $thumb; ?>" alt="<?php echo $alt; ?>" />
										<!-- <img class="thumb-hover-2" src="<?php echo $thumb_2; ?>" alt="<?php echo $alt_2; ?>" /> -->
									</a>

								<?php if( $title ): ?>

									<p class="title-front-page"><?php the_title(); ?></p>

								</div>

								<?php endif; ?>

								<?php $j = $j +1; ?>

							<?php endif; ?>
						<?php
						 elseif ( in_category('3') && ($i<=3) ) :
							;?>
							<a href="<?php the_permalink(); ?>" title="<?php echo $title; ?>">

								<div class="rectangle-home-text <?php if ($k === 2): ?>inverse-content<?php endif ;?>">
									<div class="body-h-block">
										<h4><?php the_field( 'introduction' ); ?></h4>
									</div>
								</div>
							</a>
							<?php $i= $i + 1;
								  $j = $j + 1;
								  $k = $k + 1; ?>
						<?php endif ; ?>
					</div>
			<?php endwhile; // end of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->


<?php get_sidebar(); ?>
<?php get_footer(); ?>
