<?php
/**
 * Template Name: Contact Page
 *
 * Displays content for contact page layout
 *
 * @package _mbbasetheme
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<div class="col-sm-6">

			<div class="overflow-image overflow-contact">
				<div class="image-block">
					<?php 

						$rows_images = get_field('contact_page_images');
						$first_row = $rows_images[0];
						$second_row = $rows_images[1];
						$image_1 = $first_row['image'];
						$image_2 = $second_row['image'];

						$url = $image_1['url'];
						$title = $image_1['title'];
						$alt = $image_1['alt'];
						$caption = $image_1['caption'];

						$url_2 = $image_2['url'];
						$title_2 = $image_2['title'];
						$alt_2 = $image_2['alt'];
						$caption_2 = $image_2['caption'];

						$size = 'large';
						$thumb = $image_1['sizes'][ $size ];
						$width = $image_1['sizes'][ $size . '-width' ];
						$height = $image_1['sizes'][ $size . '-height' ];

						$size_2 = 'large';
						$thumb_2 = $image_2['sizes'][ $size ];
						$width_2 = $image_2['sizes'][ $size . '-width' ];
						$height_2 = $image_2['sizes'][ $size . '-height' ];


						$mobile = $image_1['sizes'][ 'mobile' ];
						$original = $image_1['sizes'][ 'orig' ];
						$large = $image_1['sizes']['large'];

						$mobile_2 = $image_2['sizes'][ 'mobile' ];
						$original_2 = $image_2['sizes'][ 'orig' ];
						$large_2 = $image['sizes']['large'];

					if( !empty($image_1) ): ?>

						<img class="lazy" data-mobile="<?php echo $mobile; ?>" data-original="<?php echo $original; ?>" data-large="<?php echo $url;?>" alt="<?php echo $alt; ?>"/>


					<?php endif; ?>
				</div>
			</div>

		</div>


		<div class="col-sm-6">

			<div class="body-h-block">
				<div class="contact-text-primary">
					<h2>Contact us</h2>
					<?php 
						$rows_contact = get_field('employee_contact_details');
						$first_row_contact = $rows_contact[0];
						$second_row_contact = $rows_contact[1];

						$name_1 = $first_row_contact['full_name'];
						$job_1 = $first_row_contact['job_title'];
						$phone_1 = $first_row_contact['phone_number'];
						$email_1 = $first_row_contact['email'];

						$name_2 = $second_row_contact['full_name'];
						$job_2 = $second_row_contact['job_title'];
						$phone_2 = $second_row_contact['phone_number'];
						$email_2 = $second_row_contact['email'];
					?>
					<p><span><?php echo $name_1; ?></span><br><?php echo $job_1; ?><br><a href="tel:<?php echo $phone_1; ?>"><?php echo $phone_1; ?></a>&nbsp; &nbsp;|&nbsp; &nbsp;<a href="mailto:<?php echo $email_1; ?>"><?php echo $email_1; ?></a></p>
					<p><span><?php echo $name_2; ?></span><br><?php echo $job_2; ?><br><a href="tel:<?php echo $phone_2; ?>"><?php echo $phone_2; ?></a>&nbsp; &nbsp;|&nbsp; &nbsp;<a href="mailto:<?php echo $email_2; ?>"><?php echo $email_2; ?></a></p>
					

					<p><span><?php bloginfo( 'name' );?></span>  — <?php echo get_bloginfo( 'description' ); ?>&nbsp; &nbsp;|&nbsp; &nbsp;<a href="tel:<?php the_field('phone_number', 'option'); ?>"><?php the_field('phone_number', 'option'); ?></a>

					</br><?php 
						$streetAddress = get_field('street_address', 'option');
						$suburb = get_field('suburb', 'option');
						$city = get_field('city', 'option');
						$country = get_field('country', 'option');

						if( !empty($streetAddress) ): ?>

							<?php echo $streetAddress; ?>,

						<?php endif;

						if( !empty($suburb) ): ?>

							<?php echo $suburb; ?>,

						<?php endif;

						if( !empty($city) ): ?>

							<?php echo $city; ?>,

						<?php endif; 

						if( !empty($country) ): ?>

							<?php echo $country; ?>.

						<?php endif; ?>
					</p>

				</div>
			</div>

		</div>
		<style type="text/css">

.acf-map {
	width: 100%;
	height: 400px;
	border: #ccc solid 1px;
	/*margin: 20px 0;*/
}

</style>

		<?php 

		$location = get_field('map_location');

		if( !empty($location) ):
		?>
		<div class="col-sm-12 lineheight-fix">
			<div class="google-maps-container">
				<div class="acf-map">
					<div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>"></div>
				</div>
			</div>
		</div>
		<?php endif; ?>



		<div class="col-sm-6">

			<div class="body-h-block contact-text-secoondary">
				<p><?php the_field('text_field_left'); ?></p>
			</div>

		</div>


		<div class="col-sm-6 lineheight-fix">
			<div class="image-block overflow-image">
				<?php 

				if( !empty($image_2) ): ?>

					<img class="lazy" data-mobile="<?php echo $mobile_2; ?>" data-original="<?php echo $original_2; ?>" data-large="<?php echo $url_2;?>" alt="<?php echo $alt_2; ?>"/>

				<?php endif; ?>
			</div>
		</div>


		</main><!-- #main -->
	</div><!-- #primary -->

<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>

<?php get_sidebar(); ?>
<?php get_footer(); ?>
