<?php
/**
 * Template Name: About Page
 *
 * Displays content for about page layout
 *
 * @package _mbbasetheme
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">


			<div class="col-sm-6 overflow-image overflow-about">
					<?php 

					$rows_images = get_field('about_page_images');
					$first_row = $rows_images[0];
					$second_row = $rows_images[1];
					$image_1 = $first_row['image'];
					$image_2 = $second_row['image'];

					$url = $image_1['url'];
					$title = $image_1['title'];
					$alt = $image_1['alt'];
					$caption = $image_1['caption'];

					$url_2 = $image_2['url'];
					$title_2 = $image_2['title'];
					$alt_2 = $image_2['alt'];
					$caption_2 = $image_2['caption'];

					$size = 'large';
					$thumb = $image_1['sizes'][ $size ];
					$width = $image_1['sizes'][ $size . '-width' ];
					$height = $image_1['sizes'][ $size . '-height' ];

					$size_2 = 'large';
					$thumb_2 = $image_2['sizes'][ $size ];
					$width_2 = $image_2['sizes'][ $size . '-width' ];
					$height_2 = $image_2['sizes'][ $size . '-height' ];


					$mobile = $image_1['sizes'][ 'sm' ];
					$original = $image_1['sizes'][ 'orig' ];
					$large = $image_1['sizes']['large'];

					$mobile_2 = $image_2['sizes'][ 'sm' ];
					$original_2 = $image_2['sizes'][ 'orig' ];
					$large_2 = $image['sizes']['large'];

					if( !empty($image_1) ): ?>

						<img class="lazy" data-mobile="<?php echo $mobile; ?>" data-original="<?php echo $original; ?>" data-large="<?php echo $url; ?>" alt="<?php echo $alt; ?>"/>


					<?php endif; ?>
			</div>
			<div class="col-sm-6">
				<div class="body-h-block about-text-primary">
					<h2>Marx<br>Branding &amp; Packaging</h2>
					<p><?php the_field('introduction'); ?></p>
				</div>
			</div>

			<div class="col-sm-12 inverse-content our-specialty">
				<div class="body-h-block">

				<?php 
					$row_about = get_field('our_specialty');
					$first_row_about = $row_about[0];

					$packaging = $first_row_about['packaging'];
					$identity = $first_row_about['identity'];
					$form = $first_row_about['form'];
				?>

				<?php while( has_sub_field('our_specialty') ): ?>

					<?php 

						$packaging_label = get_sub_field_object('packaging');
						$identity_label = get_sub_field_object('identity');
						$form_label = get_sub_field_object('form');
					?>

				<?php endwhile; ?>



					<h3>Our Specialty</h3>

					<div class="col-sm-4 col-xs-12 about-column">
						<hr>
						<h4><?php echo $packaging_label['label']; ?></h4>
						<div class="icon icon-about-packaging"></div>
						<p><?php echo $packaging; ?></p>
					</div>

					<div class="col-sm-4 col-xs-12 about-column">
						<hr>
						<h4><?php echo $identity_label['label']; ?></h4>
						<div class="icon icon-about-identity"></div>
						<p><?php echo $identity; ?></p>
					</div>

					<div class="col-sm-4 col-xs-12 about-column">
						<hr>
						<h4><?php echo $form_label['label']; ?></h4>
						<div class="icon icon-about-form"></div>
						<p><?php echo $form; ?></p>
					</div>

				</div>
			</div>

			<div class="col-sm-12 inverse-content">

				<?php 

				if( !empty($image_2) ): ?>

					<img class="lazy" data-mobile="<?php echo $mobile_2; ?>" data-original="<?php echo $original_2; ?>" data-large="<?php echo $url_2 ;?>" alt="<?php echo $alt_2; ?>"/>

				<?php endif; ?>	
				<div class="text-overlay">
					<h5><?php the_field('studio_mantra'); ?>
						<div class="icon logo-marx-about"></div>
					</h5>
				</div>

			</div>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
