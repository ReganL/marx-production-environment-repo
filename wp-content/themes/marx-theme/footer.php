<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package _mbbasetheme
 */
?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer col-sm-12" role="contentinfo">

		<div class="footer-logo-marx col-sm-6 lineheight-fix">

			<div class="logo-marx-container">
				<div id="footer-animation"></div>
			</div>

		</div>


		<div class="footer-social-nav col-sm-6">
			<?php if( have_rows('social_links', 'option') ):
				$index = 0;

				while( have_rows('social_links', 'option') ): the_row(); 

					// vars
					$linkName = get_sub_field('link_name');
					$linkUrl = get_sub_field('link_url');
					$index = $index + 1;
					?>

					<div class="col-sm-6 col-xs-6 lineheight-fix">
						<a class="footer-link social-<?php echo $index; ?>"target="_blank" href="<?php echo $linkUrl; ?>">

							<p><?php echo $linkName; ?></p>

						</a>
					</div>

				<?php endwhile; ?>

			<?php endif; ?>

			<div class="col-sm-6 col-xs-6 lineheight-fix social-footer">
				<div class="square inverse-content social-text">
					<p>Social</p>
				</div>
			</div>


		</div>
		<div class="site-info col-sm-9 lineheight-fix">
			<div class="rectangle inverse-content">
				<p class="tagline"><?php the_field('footer_tagline', 'option'); ?></p>
				<p class="p-fine">&copy; <?php bloginfo( 'name' ); echo " "; echo date( "Y" ); ?>
				<!-- </br>21 Rawene st, Birkenhed, Auckland, New Zealand.</p> -->

				</br><?php 
						$streetAddress = get_field('street_address', 'option');
						$suburb = get_field('suburb', 'option');
						$city = get_field('city', 'option');
						$country = get_field('country', 'option');

						if( !empty($streetAddress) ): ?>

							<?php echo $streetAddress; ?>,

						<?php endif;

						if( !empty($suburb) ): ?>

							<?php echo $suburb; ?>,

						<?php endif;

						if( !empty($city) ): ?>

							<?php echo $city; ?>,

						<?php endif; 

						if( !empty($country) ): ?>

							<?php echo $country; ?>.

						<?php endif; ?>
				</p>
			</div>
		</div><!-- .site-info -->

			<div class="col-sm-3 lineheight-fix contact-footer">
				<a class="footer-link footer-contact" href="<?php echo get_page_link(43); ?>">

					<p>Contact</p>

				</a>

			</div>
		</div>
	</footer><!-- #colophon -->

</div><!-- #page -->

<?php wp_footer(); ?>


</body>
</html>
