(function($) {

    //Lazy Load
    // http://www.appelsiini.net/projects/lazyload/enabled_background.html

    var dataAttributePre;

    var ladyLoadImages = function(transition) {
        var dataAttribute;

        if ($(window).width() < 480) {
            dataAttribute = 'mobile';
            // dataAttribute = 'sq-mobile';
        } else if ($(window).width() < 1024) {
            dataAttribute = 'original';
            // dataAttribute = 'sq-original';

        } else {
            dataAttribute = 'large';
        }


        if (dataAttributePre !== dataAttribute) {
            // console.log(dataAttribute);

            $('img.lazy').lazyload({
                effect : transition,
                data_attribute: dataAttribute,
                threshold : 400
            });

            dataAttributePre = dataAttribute;
        }
    };

    $( window ).resize(function() {
        ladyLoadImages('show');
    });

    ladyLoadImages('fadeIn');

    //reveal menu icon on scroll
    var scrollcheck = true;

    window.addEventListener("scroll",function() { 
        if(window.scrollY > 245) {
            $(".menu-fixed").removeClass("menu-hide");
            $(".menu-mobile").addClass("menu-hide");
            scrollcheck = false;
        }
        else {
            $(".menu-fixed").addClass("menu-hide");
            $(".menu-mobile").removeClass("menu-hide");
            scrollcheck = true;
        }

    },false);

    $("#menu-open-mob").on("click touch", function() {
        if(scrollcheck === true) {
            $(".menu-fixed").removeClass("menu-hide");
            $(this).addClass("menu-hide");
        }
    });

    $("#menu-open").on("click touch", function() {
        if(scrollcheck === true) {
            $(this).addClass("menu-hide");
            $(".menu-mobile").removeClass("menu-hide");
        }
    });

    $("#toggle-menu").on("click touch", function() {
        if(scrollcheck === true) {
            $(".menu-fixed").addClass("menu-hide");
            $(".menu-mobile").removeClass("menu-hide");
        }
    });


//Add click function to sidebar

    $("#menu-open").on("click touch", function() {
        $(".navigation").toggleClass("navigation--open");
        $(".overlay-left").toggleClass("opacity-none");
    });

    $("#menu-open-mob").on("click touch", function() {
        $(".navigation").toggleClass("navigation--open");
        $(".overlay-left").toggleClass("opacity-none");
    });

    $("#toggle-menu").on("click touch", function() {
        $(".navigation").toggleClass("navigation--open");
        $(".overlay-left").toggleClass("opacity-none");
    });


// Animate the monogram 

    $(".site-main a:has(img)").mouseenter(function() {

        var animationIcon = $(this).children('#animation-icon');

        var i = 60;
        
        var animateFrames = setInterval(function(){
            animationIcon.removeClass().addClass("animation-align icon-monogram-frame-"+i);
            i = i - 1;
            if(i===43) {
                clearInterval(animateFrames);
            }
        }, 100);

        $(this).mouseleave(function() {
            clearInterval(animateFrames);
            animationIcon.removeClass();
        });

    });

    $(document).ready(function(){
            var k = 60;
                j = 72;

            var anim = $("#footer-animation");

            var animateFace = setInterval(function (){
                    anim.removeClass().addClass("icon-animation-face-"+j);
                    j = j - 1;
                    if(j <= 1) {
                        anim.removeClass().addClass("icon-monogram-frame-"+k);
                        k = k -1;
                    }
                    if (k === 1) {
                        j = 72;
                        k = 60;
                    }
                }, 100);

    });

    //Works the slider in the identity module

    $(document).ready(function(){

        var slidecurrent = 1,
            slidenext = 2,
            slidemax = $(".slide-identity").length,
            slideprev = 0;


        setInterval(function (){
            $("#slide-"+slidenext).addClass("visible-slide");
            $("#slide-"+slideprev).removeClass("slide-right");
            $("#slide-"+slidecurrent).addClass("slide-right").removeClass("visible-slide");


            slidecurrent = (slidecurrent >= slidemax) ? 1 : slidecurrent+1;
            slidenext = (slidenext >= slidemax) ? 1 : slidenext+1;
            slideprev = (slideprev >= slidemax) ? 1 : slideprev+1;

        }, 3000);
    });

    /*
    *  render_map
    *
    *  This function will render a Google Map onto the selected jQuery element
    *
    *  @type    function
    *  @date    8/11/2013
    *  @since   4.3.0
    *
    *  @param   $el (jQuery element)
    *  @return  n/a
    */

    function render_map( $el ) {

        // var
        var $markers = $el.find('.marker');

        // Create an array of styles.
        var styles = [
            {
                stylers: [
                    { hue: "#170c51" },
                    { saturation: -100 }
                ]
            },{
                featureType: "road",
                elementType: "geometry",
                stylers: [
                    { lightness: 100 },
                    { visibility: "simplified" }
                ]
            },{
                featureType: "road",
                elementType: "labels",
                stylers: [
                    { visibility: "off" }
                ]
            }
        ];

        // Create a new StyledMapType object, passing it the array of styles,
        // as well as the name to be displayed on the map type control.
        var styledMap = new google.maps.StyledMapType(styles, {name: "Styled Map"});

        // vars
        var args = {
            zoom        : 15,
            center      : new google.maps.LatLng(0, 0),
            mapTypeId   : google.maps.MapTypeId.ROADMAP,
            panControl: false,
            zoomControl: false,
            mapTypeControl: false,
            scaleControl: false,
            streetViewControl: false,
            overviewMapControl: false,
            scrollwheel: false
        };

        // create map               
        var map = new google.maps.Map( $el[0], args);

        //Associate the styled map with the MapTypeId and set it to display.
        map.mapTypes.set('map_style', styledMap);
        map.setMapTypeId('map_style');

        // add a markers reference
        map.markers = [];

        // add markers
        $markers.each(function(){

            add_marker( $(this), map );

        });

        // center map
        center_map( map );

    }

    /*
    *  add_marker
    *
    *  This function will add a marker to the selected Google Map
    *
    *  @type    function
    *  @date    8/11/2013
    *  @since   4.3.0
    *
    *  @param   $marker (jQuery element)
    *  @param   map (Google Map object)
    *  @return  n/a
    */

    function add_marker( $marker, map ) {
        var iconBase = '../wp-content/themes/marx-theme/assets/images/';
        // var
        var latlng = new google.maps.LatLng( $marker.attr('data-lat'), $marker.attr('data-lng') );

        // create marker
        var marker = new google.maps.Marker({
            position    : latlng,
            map         : map,
            icon: iconBase + 'Gherkin.png'
        });

        // add to array
        map.markers.push( marker );

        // if marker contains HTML, add it to an infoWindow
        if( $marker.html() )
        {
            // create info window
            var infowindow = new google.maps.InfoWindow({
                content     : $marker.html()
            });

            // show info window when marker is clicked
            google.maps.event.addListener(marker, 'click', function() {

                infowindow.open( map, marker );

            });
        }

    }

    /*
    *  center_map
    *
    *  This function will center the map, showing all markers attached to this map
    *
    *  @type    function
    *  @date    8/11/2013
    *  @since   4.3.0
    *
    *  @param   map (Google Map object)
    *  @return  n/a
    */

    function center_map( map ) {

        // vars
        var bounds = new google.maps.LatLngBounds();

        // loop through all markers and create bounds
        $.each( map.markers, function( i, marker ){

            var latlng = new google.maps.LatLng( marker.position.lat(), marker.position.lng() );

            bounds.extend( latlng );

        });

        // only 1 marker?
        if( map.markers.length == 1 )
        {
            // set center of map
            map.setCenter( bounds.getCenter() );
            map.setZoom( 15 );
        }
        else
        {
            // fit to bounds
            map.fitBounds( bounds );
        }

    }

    /*
    *  document ready
    *
    *  This function will render each map when the document is ready (page has loaded)
    *
    *  @type    function
    *  @date    8/11/2013
    *  @since   5.0.0
    *
    *  @param   n/a
    *  @return  n/a
    */

    $(document).ready(function(){

        $('.acf-map').each(function(){

            render_map( $(this) );

        });

    });

})(jQuery);